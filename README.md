**Its a conference room booking application for mobile devices.**

**Demo** - https://vimeo.com/321214584

**Tools and Technologies used :**

    - React Native
    - ES6
    - JavaScript
    - CSS
    - Redux JS
	

**Local Setup -**

  1. Clone the repository.
  3. Run command "npm install" inside project root directory to install dependencies.
  4. Run command "npm start" to run the application.


For any help, drop a mail to **varshant.14@gmail.com**