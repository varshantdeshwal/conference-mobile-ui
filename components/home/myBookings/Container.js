import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView, ActivityIndicator } from 'react-native';
import { connect } from "react-redux";
import BookingCard from './BookingCard';
import getBookings from "../../../apiCalls/getBookings";
import { onFetchBookings } from "../../../actions/index";
var MessageBarAlert = require('react-native-message-bar').MessageBar;
var MessageBarManager = require('react-native-message-bar').MessageBarManager;
class Container extends Component {
    state = { myBookings: [], showSpinner: true }
    componentDidMount() {

        this.fetchBookings();

        MessageBarManager.registerMessageBar(this.refs.alert);
    }
    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }
    errorMessage = () => {
        setTimeout(() => {
            this.setState({ showSpinner: false })
            MessageBarManager.showAlert({
                title: 'Something unusual has occurred',
                message: 'Please try after sometime',
                alertType: 'error',
                position: 'top',
                animationType: 'SlideFromTop',
                onTapped: () => MessageBarManager.hideAlert()
            });
        }, 5000)
    }
    fetchBookings = async () => {
        const myBookings = await getBookings(this.props.email);
        if (!myBookings || myBookings.error) {
            this.errorMessage();
            return;
        }
        await this.props.onFetchBookings(myBookings);
        this.setState({ showSpinner: false })
    }
    getLowerLimit = (slot) => {
        return (slot + 5) % 24;
    }
    getUpperLimit = (slot) => {
        return (slot + 6) % 24;
    }
    formatTime = (bookedSlots) => {

        let timeString = "" + this.getLowerLimit(bookedSlots[0]) + ":00";
        for (let i = 1; i < bookedSlots.length; i++) {
            if (this.getUpperLimit(bookedSlots[i - 1]) !== this.getLowerLimit(bookedSlots[i]))
                timeString += "-" + this.getUpperLimit(bookedSlots[i - 1]) + ":00, " + this.getLowerLimit(bookedSlots[i]) + ":00";
        }
        timeString += "-" + this.getUpperLimit(bookedSlots[bookedSlots.length - 1]) + ":00"
        return timeString;
    }

    render() {
        if (this.props.navigation.getParam('showMessage', false)) {
         
                MessageBarManager.showAlert({
                    // title: 'Booking successful',
                    message: 'Booking successful',
                    alertType: 'success',
                    position: 'top',
                    animationType: 'SlideFromTop',
                    onTapped: () => MessageBarManager.hideAlert()
                });
            // if (this.props.navigation.getParam('alertMessage', "Room already booked") !== "Room already booked")
            //     MessageBarManager.showAlert({
            //         // title: 'Booking successful',
            //         message: this.props.navigation.getParam('alertMessage'),
            //         alertType: 'warning',
            //         position: 'bottom',
            //         animationType: 'SlideFromRight',
            //         onTapped: () => MessageBarManager.hideAlert()
            //     });
        }
        return (
            <View style={{ flex: 1 }} >
                <ScrollView>
                    <View style={styles.container}>
                        {this.state.showSpinner ? <View style={{ paddingTop: 50 }}><ActivityIndicator size="large" color="#003366" /></View> : this.props.myBookings === null ? null : this.props.myBookings.length !== 0 ?
                            this.props.myBookings.map((booking, index) => <BookingCard roomDetails={booking.details}
                                date={booking.date}
                                bookedSlots={booking.slots} timeString={this.formatTime(booking.slots)} key={"booking_" + index}>
                            </BookingCard>) : <View style={{ paddingTop: 50 }}><Text style={styles.noDataMessage}>No booking found</Text></View>
                        }

                    </View>
                </ScrollView>
                <MessageBarAlert ref="alert" titleStyle={{
                    fontSize: 18,
                    fontWeight: 'bold'
                }}
                    messageStyle={{
                        fontSize: 16,
                        fontWeight: '200'
                    }}
                    duration={1500}
                    durationToHide={0}
                    durationToShow={200}

                    viewTopInset={10}
                    viewBottomInset={10}
                    viewLeftInset={10}
                    viewRightInset={10}

                    stylesheetSuccess={{
                        backgroundColor: '#216d34',
                        titleColor: '#ffffff',
                        messageColor: '#ffffff'
                    }}
                    stylesheetError={{
                        backgroundColor: '#ce2b2b',
                        titleColor: '#ffffff',
                        messageColor: '#ffffff'
                    }} />
            </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: "center",
        paddingHorizontal: 12,
        paddingTop: 8,
        paddingBottom: 80
    },
    noDataMessage: {
        fontSize: 18,
    }
});

function mapStateToProps(state) {
    return {
        email: state.userInfo.email,
        myBookings: state.myBookings
    };
}

export default connect(mapStateToProps, { onFetchBookings })(Container);