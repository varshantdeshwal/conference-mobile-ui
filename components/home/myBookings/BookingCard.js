import React, { Component } from 'react';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Text, View, StyleSheet } from 'react-native';
const slots = ["06-07", "07-08", "08-09", "09-10", "10-11", "11-12", "12-13", "13-14", "14-15", "15-16", "16-17", "17-18", "18-19", "19-20", "20-21", "21-22", "22-23", "23-00", "00-01", "01-02", "02-03", "03-04", "04-05", "05-06"];
const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
class BookingCard extends Component {
    state = { timeString: '', active: true }
    render() {

        return (<View style={this.state.active ? { ...styles.container, borderColor: "#444444" } : { ...styles.container, borderColor: "red" }}>

            <View style={{ ...styles.row, justifyContent: 'space-between' }}>

                <Text style={styles.roomNumber}>Room {this.props.roomDetails.roomNumber}</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                    <View style={{ ...styles.icon }}><FontAwesome style={{ fontSize: 17, color: "#4aa3d6" }}>{Icons.calendarAlt}
                    </FontAwesome></View>
                    <Text style={styles.infoText}>{this.props.date}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.icon}><FontAwesome style={{ fontSize: 17, color: "#237a3c" }}>{Icons.clock}
                </FontAwesome></View>
                <Text style={styles.infoText}>{this.props.timeString}</Text>
            </View>
            <View style={{ ...styles.row, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={styles.icon}><FontAwesome style={{ fontSize: 18, color: "#fc8019" }}>{Icons.mapMarkerAlt}</FontAwesome></View>
                    <Text style={styles.infoText}>{this.props.roomDetails.devcenter}, {this.props.roomDetails.client}, {this.props.roomDetails.location}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                    <View style={{ ...styles.icon, marginRight: 6 }}><FontAwesome style={{ fontSize: 17, color: "#ce2b2b" }}>{Icons.users}
                    </FontAwesome></View>
                    <Text style={styles.infoText}>{this.props.roomDetails.seatingcap}</Text>
                </View>
            </View>
        </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        borderWidth: 0.5,
        borderRadius: 5,
        width: "100%",
        marginTop: 14,
        padding: 10
    },
    roomNumber: {
        fontSize: 20,
        color: "#003366",
        fontWeight: 'bold'
    },
    timing: {
        height: 30,
        flexDirection: "row",
        backgroundColor: "#003366",
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        alignItems: 'center',
        paddingHorizontal: 5

    },
    name: { color: "white", fontSize: 19, fontWeight: '500' },
    timingInfo: {
        color: "black",
        fontSize: 17,
        fontWeight: '300'
    },
    row: {
        flexDirection: "row",
        paddingHorizontal: 5,
        paddingVertical: 6,
        alignItems: 'center',
        width: '100%'
    },
    icon: {
        width: 25
    },
    infoText: {
        color: "#444444",
        fontSize: 17,
        fontWeight: '300'
    }
});
export default BookingCard;