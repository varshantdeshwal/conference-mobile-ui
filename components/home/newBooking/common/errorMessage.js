var MessageBarAlert = require('react-native-message-bar').MessageBar;
var MessageBarManager = require('react-native-message-bar').MessageBarManager;
export default async ()=> await setTimeout(()=>{
MessageBarManager.showAlert({
   title: 'Something unusual has occurred',
    message: 'Please try after sometime',
    alertType: 'error',
    position: 'top',
    animationType: 'SlideFromTop',
    onTapped: () => MessageBarManager.hideAlert()
});}, 2000)