import React, { Component } from 'react';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import {
    Text,
    View,
    Dimensions,
    ActivityIndicator,
    TouchableOpacity,
    Modal,
    StyleSheet
} from 'react-native';
const { width, height } = Dimensions.get('window');
class ConfirmModal extends Component {
    state = { }
    render() { 
        return (       <Modal
            animationType="none"
            transparent={true}
            visible={this.props.showConfirmModal}
        >
            <View style={styles.container}>
                {this.props.showConfirmModal ?
                <View style={styles.confirm}>
                <View style={{ paddingVertical:"4%", alignItems:'center', justifyContent:'center'}}>
                <FontAwesome style={{ fontSize: 22, color: "#ce2b2b", paddingBottom:5 }}>{Icons.exclamationTriangle}
                </FontAwesome>
               
                    <Text style={{color:"black", fontSize:20, fontWeight:'300'}}>Meanwhile some of your selections have already been booked. Please confirm if you want to proceed with the rest or revert back the changes.</Text>
               </View>
                    <View style={{ width:'100%',paddingVertical:"4%",paddingHorizontal:"10%", flexDirection:"row", justifyContent: 'space-between', alignItems:'center'}}>
                    <TouchableOpacity onPress={(e) => {
                        this.setState({showSpinner:true});
                        this.props.revert(); 
                    }}><Text style={{ color: "#007AFF", fontSize: 18, padding: 7 }}>Revert</Text></TouchableOpacity>
                    <TouchableOpacity onPress={(e) => {
                        this.setState({showSpinner:true});
                        this.props.proceed(); 
                    }}><Text style={{ color: "#007AFF", fontSize: 18, padding: 7 }}>Proceed</Text></TouchableOpacity>
                    </View>
                   
                    
                    </View> : null}
            </View>
        </Modal> );
    }
}
 const styles=StyleSheet.create({
    confirm:{
        width:'100%',
        borderRadius:5,
        paddingHorizontal:"2%",
        backgroundColor: '#e0e0e0'
    },
    container: {
        paddingHorizontal:"7%",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.8)'
    },
 })
export default ConfirmModal;