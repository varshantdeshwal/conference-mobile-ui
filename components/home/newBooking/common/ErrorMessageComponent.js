import React, { Component } from 'react';
var MessageBarAlert = require('react-native-message-bar').MessageBar;
var MessageBarManager = require('react-native-message-bar').MessageBarManager;
const ErrorMessageComponent = () => <MessageBarAlert ref="alert" titleStyle={{
    fontSize: 16,
    fontWeight: 'bold'
}}
    messageStyle={{
        fontSize: 14,
        fontWeight: '200'
    }}
    duration={1500}
    durationToHide={0}
    durationToShow={200}

    viewTopInset={10}
    viewBottomInset={10}
    viewLeftInset={10}
    viewRightInset={10}
    stylesheetError={{
        backgroundColor: '#ce2b2b',
        titleColor: '#ffffff',
        messageColor: '#ffffff'
    }}
/>


export default ErrorMessageComponent;