import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
    ActivityIndicator,
    Modal,
    StyleSheet
} from 'react-native';
const { width, height } = Dimensions.get('window');
class Spinner extends Component {
    state = {  }
    render() { 
        return (       <Modal
            animationType="none"
            transparent={true}
            visible={this.props.showSpinner}
        >
            <View style={styles.spinnerModal}>
                {this.props.showSpinner ?
                <View style={styles.spinner}>
                <View style={{ height:'40%', paddingVertical:"4%"}}>
                    <Text style={{color:"black", fontSize:20, fontWeight:'500'}}>Please wait</Text>
               </View>
                    <View style={{height:"60%", paddingVertical:"4%"}}>
                    <ActivityIndicator size="large" color="#003366" />
                    </View>
                    </View> : null}
            </View>
        </Modal> );
    }
}
 const styles=StyleSheet.create({
    spinner:{
        height:height/8,
        borderRadius:5,
        paddingHorizontal:width/6,
        backgroundColor: '#e0e0e0'
    },
    spinnerModal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.8)'
    },
 })
export default Spinner;