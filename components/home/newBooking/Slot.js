import React, { Component } from 'react';
import { Text, TouchableOpacity, View, TextInput, StyleSheet, ScrollView } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { onSlotClicked } from "../../../actions/index";
class Slot extends Component {
    state = {  }
    selectSlot = (slot, roomId) => {
        if (this.props.selectedRooms.hasOwnProperty(roomId)) {

            const selectedSlotsCopy = this.props.selectedRooms[roomId];
            const selectedSlotIndex = selectedSlotsCopy.findIndex(selectedSlot => selectedSlot === slot);
            if (selectedSlotIndex !== -1) {
                selectedSlotsCopy.splice(selectedSlotIndex, 1);
            }
            else { selectedSlotsCopy.push(slot); }

            this.props.onSlotClicked(selectedSlotsCopy, roomId);
        }
        else
            this.props.onSlotClicked([slot], roomId);
    }
    render() { 
        console.warn("rendered");
        return (       this.props.bookedSlots.includes(this.props.index + 1) ?
            <View style={styles.bookedSlots}></View> : <TouchableOpacity
                style={this.props.selectedRooms.hasOwnProperty(this.props.roomId) && this.props.selectedRooms[this.props.roomId].includes(this.props.index + 1) ?
                        styles.availableSlot :
                        styles.selectedSlot} onPress={(e) => this.selectSlot(this.props.index + 1, this.props.roomId)}>
            </TouchableOpacity> );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,

        alignItems: 'center',

    },
    roomName: {
        marginTop: 30,
        fontWeight: 'bold',
        fontSize: 22,
    },
    selectSection: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: 'center',
        justifyContent: 'center',
    },
    slotContainer: {
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 70,
        padding: 4,
        marginVertical: 10,
        marginHorizontal: 10
    },
    availableSlot: {
        width: 40,
        height: 40,
        borderWidth: 0.7,
        borderColor: 'black',

    },
    selectedSlot: {
        width: 40,
        height: 40,
        borderWidth: 0.7,
        borderColor: 'black',
        backgroundColor: "#03969b"

    },
    bookedSlot: {
        width: 40,
        height: 40,
        borderWidth: 0.7,
        borderColor: 'black',
        backgroundColor: "grey"

    }

}) 

function mapStateToProps(state) {
    return {
        selectedRooms: state.selectedRooms
    };
}

export default connect(mapStateToProps, { onSlotClicked })(Slot);