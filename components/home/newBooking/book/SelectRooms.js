import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    Dimensions,
    Button,
    TouchableOpacity,
    ActivityIndicator,
    Modal
} from 'react-native';
import Swiper from 'react-native-swiper';
import { connect } from "react-redux";
import addBooking from "../../../../apiCalls/addBooking";
import deleteBookings from "../../../../apiCalls/deleteBookings";
import { onBooked, onBookedCleanup } from "../../../../actions";
import Availability from './Availability';
import Spinner from "../common/Spinner";
import ConfirmModal from '../common/ConfirmModal';
const { width, height } = Dimensions.get('window');



class SelectRooms extends Component {
    state = { showSpinner: false, showConfirmModal:false,  bookings:[], savedBookings:[] };
    errorMessage = () => {
        setTimeout(() => {
            this.setState({ showSpinner: false })
            MessageBarManager.showAlert({
                title: 'Something unusual has occurred',
                message: 'Please try after sometime',
                alertType: 'error',
                position: 'top',
                animationType: 'SlideFromTop',
                onTapped: () => MessageBarManager.hideAlert()
            });
        }, 5000)
    }
    book = async () => {
        if(Object.keys(this.props.selectedRooms).length===0)
        return;
        const bookingPromises = [];
        const bookings = [];
        this.setState({ showSpinner: true });
        for (let roomId in this.props.selectedRooms) {
            if (this.props.selectedRooms.hasOwnProperty(roomId)) {
                const booking = {
                    roomId: roomId,
                    slots: this.props.selectedRooms[roomId].slots,
                    date: this.props.date,
                    name: this.props.name,
                    email: this.props.email,
                    details: {
                        client: this.props.roomDetails.clients,
                        devcenter: this.props.roomDetails.devcenters,
                        location: this.props.roomDetails.locations,
                        seatingcap: this.props.roomDetails.seatingcap,
                        roomNumber: this.props.selectedRooms[roomId].roomNumber
                    },


                };
                
                bookingPromises.push(addBooking(JSON.stringify(booking)));
            }
        }
        let results = await Promise.all(bookingPromises);

        if(!results){
            this.errorMessage();
            return;
        }
        const savedBookings = [];
            results.map(booking=>{if(booking.message==="booking done"){
                bookings.push(booking.doc);
                savedBookings.push(booking.doc._id);
            }});
            console.warn(savedBookings);
        if(savedBookings.length!==results.length){
          
            this.setState({ showSpinner: false,showConfirmModal:true, savedBookings, bookings});
            return;
        }
     




        // for (const [index, result] of results.entries()) {
        //         await this.props.onBooked(bookings[index]);
        //     // errorMessages[bookings[index].details.roomNumber] = result.message;
        // }

        // let alertMessage = "Room ";
        // let anySuccessful = false;
        // for (let roomNumber in errorMessages) {
        //     if (errorMessages.hasOwnProperty(roomNumber)) {
        //         if (errorMessages[roomNumber] === "already booked") {
        //             alertMessage += roomNumber + " "
        //         }
        //         else if (errorMessages[roomNumber] === "booking done") {
        //             anySuccessful = true;
        //         }
        //     }
        // }
        // alertMessage += "already booked";
        for (const booking of bookings) {
            await this.props.onBooked(booking);
    }
        await this.props.onBookedCleanup();
        this.setState({ showSpinner: false });
        this.props.navigation.pop()
        this.props.navigation.navigate('MyBookings', { date: new Date(), showMessage: true}); 
    }
    revert=async ()=>{
        this.setState({ showConfirmModal: false, showSpinner:true});
        await deleteBookings(JSON.stringify({bookings:this.state.savedBookings}));
        await this.props.onBookedCleanup();
        this.setState({ showConfirmModal: false, savedBookings:[] });
        this.props.navigation.pop();
    }
    proceed =async ()=>{
        
        for (const booking of this.state.bookings) {
            await this.props.onBooked(booking);
    }
    this.setState({bookings:[], showConfirmModal: false })
    await this.props.navigation.pop()
    await this.props.navigation.navigate('MyBookings', { date: new Date(), showMessage: true}); 
   
    }
    render() {
        return (
            <View style={styles.container}>
             <Spinner showSpinner={this.state.showSpinner}></Spinner>
             <ConfirmModal showConfirmModal={this.state.showConfirmModal} revert={()=>this.revert()} proceed={()=>this.proceed()}>
             </ConfirmModal>
                <Swiper style={styles.wrapper}
                    dot={<View style={{ backgroundColor: 'rgba(0,0,0,.2)', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                    activeDot={<View style={{ backgroundColor: '#000', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                    paginationStyle={{ top: 7, bottom: null }} loop>

                    {this.props.availableRooms.map((room,index) => {
                        return <View style={styles.slide}  key={"room"+index} >
                            <Availability roomNumber={room.roomNumber} bookedSlots={room.bookedSlots} roomId={room._id}></Availability>
                        </View>
                    })}
                </Swiper>
                <View style={styles.buttonArea}>
                    <TouchableOpacity onPress={async (e) => {
                        await this.props.onBookedCleanup();
                        this.props.navigation.navigate('LookUp')
                    }}><Text style={{ color: "#007AFF", fontSize: 18, padding: 7 }}>Modify search</Text></TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.book} onPress={(e) => this.book()}><Text style={{ color: "white", fontWeight: "bold" }}>BOOK</Text></TouchableOpacity>
                    {/* <Button title="Back" color="#ba5b5b"></Button> */}
                </View>
            </View>
        )
    }
}
const styles = {
    container: {
        flex: 1,

    },
    spinner:{
        height:height/8,
        borderRadius:5,
        paddingHorizontal:width/6,
        backgroundColor: 'rgb(204, 204, 204)'
    },
    spinnerModal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.8)'
    },
    modifySearch: { alignItems: 'flex-start', backgroundColor: "red" },
    buttonArea: {
        // flexDirection:"row",
        // flexWrap:"wrap",
        flex: 0.2,
        alignItems: 'center',
        justifyContent: "center",
        paddingHorizontal: 15,
        borderTopWidth: .5,
        borderColor: "#afaeae"
    },
    book: {
        // backgroundColor: "#03969b",
        backgroundColor:'rgb(0, 51, 102)',
        alignItems: 'center',
        justifyContent: "center",
        width: "100%",
        height: "40%",
        color: "white",
        borderRadius: 5,
        marginBottom: 20

    },
    wrapper: {
        height: "100%"
    },

    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },

    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },

    image: {
        width,
        flex: 1,
        backgroundColor: "red"
    }
}
function mapStateToProps(state) {
    return {
        availableRooms: state.availableRooms,
        selectedRooms: state.selectedRooms,
        date: state.date.value,
        name: state.userInfo.name,
        email: state.userInfo.email,
        roomDetails: state.fields.values
    };
}

export default connect(mapStateToProps, { onBooked, onBookedCleanup })(SelectRooms);