import React, { Component } from 'react';
import { Text, TouchableOpacity, View, TextInput, StyleSheet, ScrollView, Button } from 'react-native';
import { connect } from "react-redux";
import { onSlotClicked } from "../../../../actions";
const slots = ["06-07", "07-08", "08-09", "09-10", "10-11", "11-12", "12-13", "13-14", "14-15", "15-16", "16-17", "17-18", "18-19", "19-20", "20-21", "21-22", "22-23", "23-00", "00-01", "01-02", "02-03", "03-04", "04-05", "05-06"];
class Availability extends Component {
    state = {
        selectedRooms: {}
    }
    selectSlot = (slot, roomId, roomNumber) => {
        let selectedSlotsCopy = [];
        if (this.props.selectedRooms.hasOwnProperty(roomId)) {

            selectedSlotsCopy = this.props.selectedRooms[roomId].slots;
            const selectedSlotIndex = selectedSlotsCopy.findIndex(selectedSlot => selectedSlot === slot);
            if (selectedSlotIndex !== -1) {
                selectedSlotsCopy.splice(selectedSlotIndex, 1);
            }
            else { selectedSlotsCopy.push(slot); }

            this.props.onSlotClicked(selectedSlotsCopy, roomId, roomNumber);
        }
        else {
            selectedSlotsCopy.push(slot)
            this.props.onSlotClicked([slot], roomId, roomNumber);
        }
        this.setState(prevState => {
            let selectedRoomsCopy = prevState.selectedRooms;
            selectedSlotsCopy.length === 0 ?
                delete selectedRoomsCopy[roomId] :
                selectedRoomsCopy[roomId] = selectedSlotsCopy;
            return { selectedRooms: selectedRoomsCopy };
        })
    }
    render() {
        return (<View style={styles.container}>
            <Text style={styles.roomName}>Room {this.props.roomNumber}</Text>
            <View style={styles.separator}></View>
            <ScrollView>
                <View style={styles.selectSection}>
                    {slots.map((slot, index) => <View style={styles.slotContainer} key={"slot" + index}><Text>{slot}</Text>
                        {this.props.bookedSlots.includes(index + 1) ?
                            <View style={styles.bookedSlot}></View> : <TouchableOpacity
                                style={this.state.selectedRooms.hasOwnProperty(this.props.roomId) && this.state.selectedRooms[this.props.roomId].includes(index + 1) ?
                                    styles.selectedSlot :
                                    styles.availableSlot} onPress={(e) => this.selectSlot(index + 1, this.props.roomId, this.props.roomNumber)}>
                            </TouchableOpacity>}
                    </View>
                    )}
                </View>
            </ScrollView>
        </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,

        alignItems: 'center',

    },
    roomName: {
        marginTop: 24,
        fontWeight: 'bold',
        fontSize: 22,
        marginBottom: 5,
        color: '#003366'
    },
    selectSection: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom:40
    },
    slotContainer: {
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 70,
        padding: 4,
        marginVertical: 10,
        marginHorizontal: 10
    },
    availableSlot: {
        width: 40,
        height: 40,
        borderWidth: 0.7,
        borderColor: 'black',

    },
    selectedSlot: {
        width: 40,
        height: 40,
        borderWidth: 0.7,
        borderColor: 'black',
        backgroundColor: "rgb(96, 189, 153)"

    },
    bookedSlot: {
        width: 40,
        height: 40,
        borderWidth: 0.7,
        borderColor: 'black',
        backgroundColor: "#afafaf"

    },
    separator: {
        height: 1,
        width: '100%',
        borderTopWidth: .5,
        borderColor: "#afaeae"
    }

})
function mapStateToProps(state) {
    return {
        selectedRooms: state.selectedRooms
    };
}

export default connect(mapStateToProps, { onSlotClicked })(Availability);