import { createStackNavigator} from "react-navigation";
import LookUp from "./lookUp/LookUp";
import SelectRooms from "./book/SelectRooms";

const NewBooking = createStackNavigator(
    {
        LookUp: {
            screen: LookUp,
            navigationOptions: {
                header: null,
            }
        },
        SelectRooms: {
            screen: SelectRooms,
            navigationOptions: {
                header: null,
            }
        },
    }
);
 
export default NewBooking;
