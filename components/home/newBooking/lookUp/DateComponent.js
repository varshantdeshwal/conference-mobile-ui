import React, { Component } from 'react';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { connect } from "react-redux";
import { Text, TouchableOpacity, View, TextInput, StyleSheet } from 'react-native';
import { setDatePickerVisibility, onDatePicked } from "../../../../actions";

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
class DateComponent extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.fieldName}>
                    <Text style={styles.label}>Date</Text>
                </View>
                <TouchableOpacity style={styles.selectBox} onPress={(e) => this.props.setDatePickerVisibility(true)}>
                    <TextInput
                        pointerEvents='none'
                        placeholder="Select date"
                        editable={false}
                        style={styles.input}
                        value={this.props.date}
                    />
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.props.visibility}
                    onConfirm={(date) => this.props.onDatePicked("" + date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear(), false)}
                    onCancel={(e) => this.props.setDatePickerVisibility(false)}
                    minimumDate={new Date()}
                //   maximumDate={new Date(new date().setTime(date.getTime() + (days * 24 * 60 * 60 * 1000)))}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',

    },
    fieldName: {
        flex: 0.5,
        textAlign: 'right',
        height: 30,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: 10
    },
    input: {
        height: '100%',
        width: '100%',
        fontWeight: '300',
        fontSize: 17
    },
    selectBox: {
        flex: 0.5,
        height: 30,
        borderBottomWidth: 0.6,

        borderColor: '#4f5051',
        paddingHorizontal: 6,

    },
    label: {
        fontWeight: '500',
        fontSize: 17
    }
});
function mapStateToProps(state) {
    return {
        date: state.date.value,
        visibility: state.date.isDateTimePickerVisible
    };
}
export default connect(mapStateToProps, { setDatePickerVisibility, onDatePicked })(DateComponent);