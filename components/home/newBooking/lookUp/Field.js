import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Text, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import { onSelectorPressed } from '../../../../actions/index';
class Field extends Component {
    state = {}
    render() {
        return (<View style={styles.container}>
            <View style={styles.fieldName}>
                <Text style={styles.label}>{this.props.fieldName}</Text></View>
            <TouchableOpacity style={styles.selectBox} onPress={(e) => {
                if (this.props.fields[this.props.identifier] === null) return;
                this.props.onSelectorPressed(this.props.identifier, !this.props.selectionListVisible)
            }}>
                <TextInput
                    pointerEvents='none'
                    placeholder={this.props.values[this.props.identifier] || null}

                    editable={false}
                    style={styles.input}
                    value={this.props.values[this.props.identifier] !== "Select an option" ? this.props.values[this.props.identifier] : null}
                />
            </TouchableOpacity>
        </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',

    },
    fieldName: {
        flex: 0.5,
        textAlign: 'right',
        height: 30,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: 10
    },
    input: {
        height: '100%',
        width: '100%',
        fontWeight: '300',
        fontSize: 17
    },
    selectBox: {
        flex: 0.5,
        height: 30,
        borderBottomWidth: 0.6,

        borderColor: '#4f5051',
        paddingHorizontal: 6,

    },
    label: {
        fontWeight: '500',
        fontSize: 17
    }
});
function mapStateToProps(state) {
    return {
        selectionListVisible: state.selectionList.visibility,
        values: state.fields.values,
        fields: state.fields
    };
}
export default connect(mapStateToProps, { onSelectorPressed: onSelectorPressed })(Field);