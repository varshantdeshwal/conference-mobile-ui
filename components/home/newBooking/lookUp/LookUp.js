import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import fetchAvailableRooms from "../../../../apiCalls/fetchAvailableRooms";
import fetchRoomStatus from "../../../../apiCalls/fetchRoomStatus";
import Field from './Field';
import SelectionList from './SelectionList';
import DateComponent from './DateComponent';
import { onSearch, onBookedCleanup } from "../../../../actions"
import Spinner from '../common/Spinner';
import ErrorMessageComponent from '../common/ErrorMessageComponent';
import errorMessage from "../common/errorMessage"
var MessageBarAlert = require('react-native-message-bar').MessageBar;
var MessageBarManager = require('react-native-message-bar').MessageBarManager;


class LookUp extends Component {
    state = { showSpinner: false }
    componentDidMount() {
        MessageBarManager.registerMessageBar(this.refs.alert);
    }
    componentWillUnmount() {
        MessageBarManager.unregisterMessageBar();
    }

    verifyFields = () => {
        let valid = true;
        Object.keys(this.props.values).map(key => {
            if (this.props.values[key] === "" || this.props.values[key] === "Select an option") valid = false;
        });
        return valid;
    }
    errorMessage = () => {
        setTimeout(() => {
            this.setState({ showSpinner: false })
            MessageBarManager.showAlert({
                title: 'Something unusual has occurred',
                message: 'Please try after sometime',
                alertType: 'error',
                position: 'top',
                animationType: 'SlideFromTop',
                onTapped: () => MessageBarManager.hideAlert()
            });
        }, 5000)
    }
    find = async () => {
        this.setState({ showSpinner: true });
        await this.props.onBookedCleanup();
        const searchLocation = this.props.fields.locations.find(loc => { return loc.location === this.props.values.locations; });
        const rooms = await fetchAvailableRooms(searchLocation._id, parseInt(this.props.values.seatingcap));

        if (!rooms || rooms.error) {
            this.errorMessage();
            return;
        }

        let promises = rooms.map(room => { return fetchRoomStatus(room._id, this.props.date) });
        let slots = await Promise.all(promises);
        const availableRooms = [];
        let canShowAvailability = true;
        for (const [index, room] of rooms.entries()) {
            if (!slots || !slots[index] || slots[index].error) {
                canShowAvailability = false;
                this.errorMessage();
                break;
            }
            var details = {
                _id: room._id,
                roomNumber: room.roomNumber,
                seatingCpacity: room.seatingCapacity,
                bookedSlots: slots[index]
            };
            availableRooms.push(details);

        }
        if (canShowAvailability) {
            await this.props.onSearch(availableRooms);
            this.setState({ showSpinner: false });
            this.props.navigation.navigate('SelectRooms');
        }
    };

    render() {
        let isReadyToFind = this.verifyFields();
        return (
            <View style={styles.container}>
                <Spinner showSpinner={this.state.showSpinner}></Spinner>
                <View style={styles.heading}>
                    <Text style={{ fontSize: 26, fontWeight: '100' }}>Conference Room Lookup</Text>
                </View>
                <View style={styles.fieldsContainer}>

                    <Field identifier='devcenters' fieldName='Development Center'></Field>
                    <Field identifier='clients' fieldName='Client'></Field>
                    <Field identifier='locations' fieldName='Location'></Field>
                    <Field identifier='seatingcap' fieldName='Seating Capacity'></Field>
                    <DateComponent></DateComponent>
                    <SelectionList></SelectionList>
                    {isReadyToFind ? <TouchableOpacity style={styles.findButton} onPress={(e) => this.find()}><Text style={{ color: "white", fontWeight: 'bold' }}>SEARCH</Text></TouchableOpacity> : null}

                </View>
                <MessageBarAlert ref="alert" titleStyle={{
                    fontSize: 16,
                    fontWeight: 'bold'
                }}
                    messageStyle={{
                        fontSize: 14,
                        fontWeight: '200'
                    }}
                    duration={1500}
                    durationToHide={0}
                    durationToShow={200}

                    viewTopInset={10}
                    viewBottomInset={10}
                    viewLeftInset={10}
                    viewRightInset={10}
                    stylesheetError={{
                        backgroundColor: '#ce2b2b',
                        titleColor: '#ffffff',
                        messageColor: '#ffffff'
                    }}
                />
            </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    heading: {
        paddingVertical: 15,
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: "#afaeae"

    },
    fieldsContainer: {
        alignItems: 'center',
        paddingTop: 20
    },
    findButton: {
        width: 90,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#003366",
        borderRadius: 5,
        marginTop: 30,
    }
});
function mapStateToProps(state) {
    return {
        values: state.fields.values,
        fields: state.fields,
        date: state.date.value
    };
}

export default connect(mapStateToProps, { onSearch, onBookedCleanup })(LookUp);