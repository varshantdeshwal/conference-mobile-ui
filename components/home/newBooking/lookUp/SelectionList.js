import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, ActivityIndicator, Dimensions, View, StyleSheet, FlatList, } from 'react-native';
import { connect } from "react-redux";
import { onSelectorPressed } from '../../../../actions';
import fetchDevcenters from '../../../../apiCalls/fetchDevcenters';
import fetchClients from '../../../../apiCalls/fetchClients';
import fetchLocations from '../../../../apiCalls/fetchLocations';
import fetchSeatingCapacity from '../../../../apiCalls/fetchSeatingCapacity';
import { onLoadRequest, onDropdownSelected, onSelectionListClose } from '../../../../actions'

const { width, height } = Dimensions.get('window');
class SelectionList extends Component {
  onItemPress = (id, value) => {
    switch (this.props.fieldName) {
      case "devcenters":
      this.props.onDropdownSelected([], "clients", value)
        fetchClients(id).then(data => {
          this.props.onDropdownSelected(data, "clients", value);
        });
        break;
      case "clients":
      this.props.onDropdownSelected([], "locations", value)
        fetchLocations(id).then(data => {
          this.props.onDropdownSelected(data, "locations", value);
        });
        break;
      case "locations":
      this.props.onDropdownSelected([], "seatingcap", value)
        fetchSeatingCapacity(id).then(data => {
          this.props.onDropdownSelected(data, "seatingcap", value);
        });
        break;
      case 'seatingcap':
        this.props.onDropdownSelected([], "complete", value.toString());
        break;

      default:
        break;
    }
    this.props.onSelectorPressed(this.props.fieldName, !this.props.selectionListVisible);

  }
  componentDidMount() {
    fetchDevcenters().then(data => {
      this.props.onLoadRequest(data);
    });
  }
  formatData = () => {
    switch (this.props.fieldName) {
      case "devcenters":
      case "clients":
        return this.props.fields[this.props.fieldName];
      case "locations":
        const locationData = [];
        for (item of this.props.fields[this.props.fieldName]) {
          locationData.push({ name: item.location, _id: item._id })
        }
        return locationData;
      case "seatingcap":
        const seatingcapData = [];
        for (item of this.props.fields[this.props.fieldName]) {
          seatingcapData.push({ name: item, _id: item.toString() })
        }
        return seatingcapData;
      default:
        return;
    }
  }
  render() {
    return (
      <View>
        <Modal
          animationType="none"
          transparent={true}
          visible={this.props.selectionListVisible}
        >

          <TouchableOpacity onPress={(e) => this.props.onSelectionListClose(false)} style={styles.safeContainer} activeOpacity={0}>

            <View style={styles.content}>
              {this.props.fields[this.props.fieldName] === null || this.props.fields[this.props.fieldName] === 'Select an option' || this.props.fields[this.props.fieldName].length === 0 ?
                <ActivityIndicator size="large" color="#003366" /> :
                <FlatList
                  ItemSeparatorComponent={Separator}
                  data={this.formatData()}
                  keyExtractor={item => item._id}
                  renderItem={({ item }) => <TouchableOpacity
                    style={styles.listItem}
                    onPress={(e) => this.onItemPress(item._id, item.name)}

                  >
                    <Text style={styles.option}>{item.name}</Text>

                  </TouchableOpacity>

                  }
                />
              }
            </View>

          </TouchableOpacity>

        </Modal>

      </View>
    );
  }
}
const Separator = () => {
  return (<View style={styles.separator}></View>)
}
function mapStateToProps(state) {
  return {
    selectionListVisible: state.selectionList.visibility,
    fieldName: state.selectionList.fieldName,
    fields: state.fields
  };
}


const styles = StyleSheet.create({
  safeContainer: {
    width: "100%",
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)'
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    minHeight:'10%',
    maxHeight: '60%',
    width: width * .7,
    borderRadius: 5,
    backgroundColor: '#e0e0e0'
  },
  listItem: {
paddingVertical:14,
paddingHorizontal:7,
    width: width * .7,
    justifyContent: "center",
    alignItems: 'center',
    flexDirection:'row',
    flexWrap:'wrap'

  },
  separator: {
    width: '95%',
    borderTopWidth: 1,
    borderColor: '#4f5051',
    marginHorizontal:"2.5%",
 
  },
  option: {
    fontSize: 18,
    fontWeight: '600'
  }
});
export default connect(mapStateToProps,   {
  onSelectorPressed,
  onLoadRequest,
  onDropdownSelected,
  onSelectionListClose
})(SelectionList);