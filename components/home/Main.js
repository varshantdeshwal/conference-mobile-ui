import { StyleSheet} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation';
import NewBookingContainer from './newBooking/container';
import MyBookingsContainer from './myBookings/Container';
const Tabs = createMaterialTopTabNavigator(
    {
        NewBooking: {
            screen: NewBookingContainer,
            navigationOptions:{
                title: 'New Booking'
            }
        },
        MyBookings: { screen: MyBookingsContainer,
            navigationOptions:{
                title: 'My Bookings'
            } }
    },
    {
        tabBarOptions: {
            labelStyle: {
                fontSize: 14,
            },
            style: {
                backgroundColor: '#003366',
            },
        },
        lazy: true
    });

export default Tabs;

