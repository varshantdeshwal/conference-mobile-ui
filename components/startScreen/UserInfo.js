import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View } from 'react-native';
import { connect } from "react-redux";
import { onUserFieldChange } from '../../actions/index';
class UserInfo extends Component {
  render() {
    return (<View style={styles.container}>
      <Text style={styles.fieldName}>NAME</Text>
      <TextInput
        style={styles.fieldText}
        onChangeText={text => this.props.onUserFieldChange("name", text)}
        value={this.props.name}
        placeholder="Enter your name"

      />
      <Text style={{ height: 50 }}>
      </Text>
      <Text style={styles.fieldName}>EMAIL</Text>
      <TextInput
        style={styles.fieldText}
        onChangeText={text => this.props.onUserFieldChange("email", text)}
        value={this.props.email}
        placeholder="Enter your e-mail"
        autoCapitalize="none"
      />
    </View>);
  }
}
const styles = StyleSheet.create({
  fieldText: {
    height: 40,
    borderBottomColor: '#bfbfbf',
    borderBottomWidth: 1,
    color: '#2b2b2b',
    fontSize: 18,
    fontWeight: '300',
    marginTop: 10
  },
  fieldName: {
    color: 'black',
    fontWeight: '500',
    fontSize: 15
  },
  container: {
    width: '100%',
    flex: .3,
    padding: 10,
    marginTop: 20,
  }
});

function mapStateToProps(state) {
  return {
    name: state.userInfo.name,
    email: state.userInfo.email
  };
}

export default connect(
  mapStateToProps,
  { onUserFieldChange }
)(UserInfo);