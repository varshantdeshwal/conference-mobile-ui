import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, View, Text } from 'react-native';
import { connect } from "react-redux";
import UserInfo from './UserInfo';
import AppName from './AppName';
import Proceed from './Proceed';
import { onResetStore } from "../../actions/index";

class Main extends Component {
    state = { message: "" }
    validateAndProceed = async () => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (this.props.name === null || this.props.email === null || this.props.name === "" || this.props.email === "") {
            this.setState({ message: "*all fields are required" });
        }
        else if (re.test(String(this.props.email).toLowerCase())) {
            await this.props.onResetStore();
            this.props.navigation.navigate('Home');
            this.setState({ message: "" });

        }
        else {
            this.setState({ message: "*enter valid email" });
        }
    }
    render() {

        return (
            <View style={styles.container}>
                <SafeAreaView style={styles.safeContainer}>
                    <AppName></AppName>
                    <UserInfo></UserInfo>
                    <Text style={styles.validationMessage}>{this.state.message}</Text>
                    <Proceed proceed={() => this.validateAndProceed()}></Proceed>

                </SafeAreaView>

            </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    safeContainer: {
        width: '100%',
        flex: 1,
        alignItems: 'center',
    },
    validationMessage: {
        color: "red"
    }
});
function mapStateToProps(state) {
    return {
        name: state.userInfo.name,
        email: state.userInfo.email,
    };
}

export default connect(mapStateToProps, { onResetStore })(Main);