import React, { Component } from 'react';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { StyleSheet, View,  TouchableOpacity } from 'react-native';
class Proceed extends Component {
    state = {}
    render() {
        return (<View style={styles.container}>
            <TouchableOpacity style={styles.proceed} onPress={this.props.proceed} activeOpacity={0.5}><FontAwesome style={{ fontSize: 20, color: "white" }}>{Icons.arrowRight}</FontAwesome></TouchableOpacity>
        </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        flex: .2,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',


    },
    proceed: {
        color: 'white',
        paddingHorizontal: 30,
        paddingVertical: 10,
        backgroundColor: '#003366',
        borderRadius: 20
    }
});
export default Proceed;