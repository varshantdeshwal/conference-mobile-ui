import React, { Component } from 'react';
import { StyleSheet, TextInput, Text, View } from 'react-native';
class AppName extends Component {
    state = {}
    render() {
        return (<View style={styles.container}>
            <Text style={styles.appName}>Conference Room</Text>
            <Text style={styles.appName}>Lookup</Text>
        </View>);
    }
}
const styles = StyleSheet.create({
    container: {
        flex: .2,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',

        paddingVertical: 20

    },
    appName: {
        color: '#003366',
        fontWeight: '400',
        fontSize: 38,
    }
});
export default AppName;