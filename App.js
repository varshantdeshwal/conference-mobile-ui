import React, { Component } from 'react';
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "./reducers";
import { createStackNavigator, createAppContainer } from "react-navigation";
import StartScreen from './components/startScreen/Main';
import HomeScreen from './components/home/Main'

const store = createStore(rootReducer);


const AppNavigator = createStackNavigator(
  {
    Start: {
      screen: StartScreen,
      navigationOptions: {
        header: null,
      }
    },
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'Home'
      }
    },
  },
  {
    initialRouteName: 'Start',
  }
);
const AppContainer = createAppContainer(AppNavigator);
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>

        <AppContainer></AppContainer>

      </Provider>
    );
  }
}

