

export default selectedRooms = (state = {}, action) => {
    switch (action.type) {
        case 'ON_SLOT_CLICKED':
            let selectedRooms = state;
            action.selectedSlots.length === 0 ?
                delete selectedRooms[action.roomId] :
                (selectedRooms[action.roomId] = {slots:action.selectedSlots, roomNumber:action.roomNumber});
            return selectedRooms;
        case "ON_BOOKED_CLEANUP":
        return {}
        case 'ON_RESET_STORE':
        return {}
        default:
            return state;
    }

}