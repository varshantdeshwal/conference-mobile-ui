import { combineReducers } from "redux";
import userInfo from './userInfo';
import fields from "./populateDropdowns";
import selectionList from './selectionList';
import date from "./captureDate";
import availableRooms from "./availableRooms";
import selectedRooms from "./selectedRooms";
import myBookings from "./myBookings"
export default combineReducers({ userInfo, fields, selectionList,date, availableRooms,selectedRooms, myBookings });