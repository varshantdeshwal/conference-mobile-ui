const availableRooms = (state = [], action) => {
  switch (action.type) {
    case "ON_SEARCH":
      return action.data;
    case "ON_BOOKED_CLEANUP":
      return [];
    case "ON_RESET_STORE":
      return []
    default:
      return state;
  }
};
export default availableRooms;
