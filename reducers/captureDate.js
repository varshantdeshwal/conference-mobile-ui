let date=new Date();
const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
let initialState={
    value:""+date.getDate()+"-"+months[date.getMonth()]+"-"+date.getFullYear(),
    isDateTimePickerVisible: false
}
export default date = (state =initialState, action) => {
    switch (action.type) {
        case 'ON_DATE_PICKED':
            return {value:action.value, isDateTimePickerVisible:action.visibility}
        case 'SET_DATE_PICKER_VISIBILITY':
        return {value:state.value, isDateTimePickerVisible:action.visibility}
        case "ON_RESET_STORE":
        return initialState
        default:
            return state;
    }

}