const initialState = {
  devcenters:'Select an option',
  clients: null,
  locations: null,
  seatingcap: null,
  values: { devcenters: 'Select an option', clients: null, locations: null, seatingcap: null }
};

const fields = (state = initialState, action) => {
  switch (action.type) {
    case "ON_DROPDOWN_SELECTED":
      switch (action.fieldToPopulate) {
        case "clients":
          return {
            devcenters: state.devcenters,
            clients: action.data,
            locations: null,
            seatingcap: null,
            values: { devcenters: action.currentValue, clients: 'Select an option', locations: null, seatingcap: null }
          };

        case "locations":
          return {
            devcenters: state.devcenters,
            clients: state.clients,
            locations: action.data,
            seatingcap: null,
            values: { devcenters: state.values.devcenters, clients: action.currentValue, locations: 'Select an option', seatingcap: null }
          };

        case "seatingcap":
          return {
            devcenters: state.devcenters,
            clients: state.clients,
            locations: state.locations,
            seatingcap: action.data,
            values: { devcenters: state.values.devcenters, clients: state.values.clients, locations: action.currentValue, seatingcap: 'Select an option' }
          };
        case "complete":
          return {
            devcenters: state.devcenters,
            clients: state.clients,
            locations: state.locations,
            seatingcap: state.seatingcap,
            values: { devcenters: state.values.devcenters, clients: state.values.clients, locations: state.values.locations, seatingcap: action.currentValue }
          };
        default:
          return state;
      }

    case "ON_LOAD":
      return {
        devcenters: action.data,
        clients: state.clients,
        locations: state.locations,
        seatingcap: state.seatingcap,
        values: state.values
      };
case "ON_RESET_STORE":
return initialState
    default:
      return state;
  }
};
export default fields;
