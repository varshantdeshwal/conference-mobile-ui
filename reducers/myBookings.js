export default selectedRooms = (state = null, action) => {
    switch (action.type) {
        case 'ON_FETCH_BOOKINGS':
            return action.data.reverse();
        case "ON_BOOKED":
            const myBookingsCopy=state || [];
            myBookingsCopy.unshift(action.booking);
            return myBookingsCopy;
            case "ON_RESET_STORE":
            return null;
        default:
            return state;
    }

}