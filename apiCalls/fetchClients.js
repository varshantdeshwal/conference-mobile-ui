import url from "./url";
var result = async function getClientsCall(id) {
  const res = await fetch(
    url+"clients/" + id,

    {
      method: "GET"
    }
  );
  const data = await res.json();
  return data;
};

export default result;
