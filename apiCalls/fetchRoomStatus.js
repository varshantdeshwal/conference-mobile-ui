import url from "./url";
var result = async function getClientsCall(id, date) {
  const res = await fetch(
    url+"bookings/" + id + "/" + date,

    {
      method: "GET"
    }
  );
  const data = await res.json();

  return data;
};

export default result;
