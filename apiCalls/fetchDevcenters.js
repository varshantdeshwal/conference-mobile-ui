import url from "./url";
var result = async function getClientsCall() {
  const res = await fetch(
    url+"devcenters/",

    {
      method: "GET"
    }
  );
  const data = await res.json();

  return data;
};

export default result;
