import url from "./url";
var result = async function insertBookCall(data) {
  const res = await fetch(
    url+"bookings/",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      },
      body: data
    }
  );
  const booking = await res.json();
  return booking;
};

export default result;
