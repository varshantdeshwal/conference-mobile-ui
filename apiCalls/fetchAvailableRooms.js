import url from "./url";
var result = async function getCall(id, capacity) {
  const res = await fetch(
    url+"rooms/getRoomsWithCapacity/" + id + "/" + capacity,

    {
      method: "GET"
    }
  );

  const data = await res.json();

  return data;
};

export default result;
