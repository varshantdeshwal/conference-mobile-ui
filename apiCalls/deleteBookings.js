import url from "./url";
var result = async function deleteBookings(data) {
  const res = await fetch(
    url+"bookings/",

    {
      method: "DELETE",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      },
      body: data
    }
  );
  const booking = await res.json();
  return booking;
};

export default result;