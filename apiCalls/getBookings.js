import url from "./url";
var result = async function getBookings(email) {
    const res = await fetch(
      url+"bookings/"+email,
  
      {
        method: "GET"
      }
    );
    const data = await res.json();
  
    return data;
  };
  
  export default result;