import url from "./url";
var result = async function getClientsCall(id) {
  const res = await fetch(
    url+"locations/" + id,

    {
      method: "GET"
    }
  );
  const data = await res.json();

  return data;
};

export default result;
