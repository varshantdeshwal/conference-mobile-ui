export const onUserFieldChange = (field, value) => {
  return {
    type: "ON_USER_FIELD_CHANGE",
    field,
    value
  };
};
export const onDropdownSelected = (data, fieldToPopulate, currentValue) => {
  return {
    type: "ON_DROPDOWN_SELECTED",
    data,
    fieldToPopulate,
    currentValue
  };
};
export const onLoadRequest = data => {
  return {
    type: "ON_LOAD",
    data
  };
};
export const onSelectorPressed = (fieldName, visibility) => {
  return {
    type: "ON_SELECTOR_PRESSED",
    fieldName,
    visibility
  };
}
export const onSelectionListClose = ( visibility) => {
  return {
    type: "ON_SELECTION_LIST_CLOSE",
    visibility
  };
}
export const onDatePicked = (value, visibility) => {
  return {
    type: "ON_DATE_PICKED",
   value,
    visibility
  };
}
export const setDatePickerVisibility = (visibility) => {
  return {
    type: "SET_DATE_PICKER_VISIBILITY",
    visibility
  };
}
export const onSearch = data => {
  return {
    type: "ON_SEARCH",
    data
  };
};
export const onSlotClicked = (selectedSlots,roomId, roomNumber) => {
  return {
    type: "ON_SLOT_CLICKED",
    selectedSlots,
    roomId,
    roomNumber
  };
};
export const onBooked=(booking)=>{
  return {
    type:"ON_BOOKED",
    booking
  }
}
export const onBookedCleanup=()=>{
  return {
    type:"ON_BOOKED_CLEANUP"
  }
}
export const onFetchBookings=(data)=>{
  return {
    type:"ON_FETCH_BOOKINGS",
    data

  }
}
export const onResetStore=()=>{
  return {
    type:"ON_RESET_STORE"

  }
}